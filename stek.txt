#include "iostream"
#include "cstring"

struct Node {
  Node *next = nullptr, *prev = nullptr;
  int val;
};

struct Stack {
  Node *first = nullptr, *last = nullptr;
  int size;
};

Stack CreateStack() {
  Stack s;
  s.size = 0;
  return s;
}

void Push(Stack &d, int value) {
  Node *n = new Node;
  n->val = value;
  if (d.first == nullptr) {
    d.first = d.last = n;
  } else {
    n->next = d.last;
    d.last->prev = n;
    d.last = n;
  }
  d.size++;
}

int Pop(Stack &d) {
  Node *del = d.first;
  int n = del->val;
  d.first = del->prev;
  delete del;
  d.size--;
  return n;
}
void Clear(Stack &d) {
  Node *del;
  while (d.first != nullptr) {
    del = d.first;
    d.first = del->prev;
    delete del;
    d.size--;
  }
}

int main() {
  int n;
  Stack d = CreateStack();
  char command[100];
  while (strcmp(command, "exit") != 0) {
    std::cin >> command;
    if (strcmp(command, "push") == 0) {
      std::cin >> n;
      Push(d, n);
      std::cout << "ok" << std::endl;
    } else if (strcmp(command, "pop") == 0) {
      std::cout << Pop(d) << std::endl;
    } else if (strcmp(command, "front") == 0) {
      std::cout << d.first->val << std::endl;
    } else if (strcmp(command, "size") == 0) {
      std::cout << d.size << std::endl;
    } else if (strcmp(command, "clear") == 0) {
      Clear(d);
      std::cout << "ok" << std::endl;
    }
  }
  std::cout << "bye";
  return 0;
}